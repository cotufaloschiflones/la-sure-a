<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<title>La Sureña</title>
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
	<link rel="stylesheet" href="css/styles.css" />
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/fontello-ie7.css">
    <![endif]-->
	
</head>
<body>

<div class="body" data-role="page">

	<header data-role="header">
		<a href="index.html" data-role="button" data-icon="arrow-l" data-rel="back" data-inline="true" data-theme="c"><span class="enfasis">V</span>olver</a>
		<h1 class="header-top"><span class="enfasis">L</span>a <span class="enfasis">S</span>ureña</h1>
	</header><!-- /header -->

	<section data-role="content" class="content">
		<h1><span class="enfasis">N</span>uestras <span class="enfasis">P</span>romos</h1>		
		
		<ul class="listado promos">
			<li>
					<h3>Martes al Cubo</h3>
					<p>Los martes por la compra de un Tinglao te regalamos un cubo.</p>
					<aside>
						<a href="" class="left">
							<i class="icon-thumbs-up"></i> 
						</a>
						<a href="" class="right">
							<i class="icon-twitter"></i>
						</a> 
					</aside>
			</li>

			<li>
					<h3>Houston, tenemos un cubo</h3>
					<p>Los martes por la compra de un Tinglao te regalamos un cubo.</p>
					<aside>
						<a href="" class="left">
							<i class="icon-thumbs-up"></i> 
						</a>
						<a href="" class="right">
							<i class="icon-twitter"></i>
						</a> 
					</aside>
			</li>

			
		</ul>
		
	</section><!-- /content -->

	

</div><!-- /page -->



<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.8.3.min.js"><\/script>')</script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(window).resize(function(){
			var ancho = $(window).width();
			var alto = $(window).height();
			console.log('Ancho: ' + ancho);
			console.log('Alto: ' + alto);
		})

	});
	
</script>
</body>
</html>