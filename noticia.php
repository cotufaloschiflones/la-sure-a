<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<title>La Sureña</title>
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
	<link rel="stylesheet" href="css/styles.css" />
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/fontello-ie7.css">
    <![endif]-->
	
</head>
<body>

<div class="body" data-role="page">

	<header data-role="header">
		<a href="index.html" data-role="button" data-icon="arrow-l" data-rel="back" data-inline="true" data-theme="c"><span class="enfasis">V</span>olver</a>
		<h1 class="header-top"><span class="enfasis">L</span>a <span class="enfasis">S</span>ureña</h1>
	</header><!-- /header -->

	<section data-role="content" class="content noticia">
		<h2 class="noticia"><span class="enfasis">C</span>ervecería La Sureña abre 4 restaurantes en una semana.</h2>		
		<img src="http://jquerymobile.com/demos/1.2.0/docs/lists/images/album-bb.jpg" class="noticia">
		<p>
			Restalia, primer grupo español en franquicia 
			ultimarca, inaugura hoy  nuevo restaurante de enseña Cervecería La Sureña en la localidad 
			alicantina de Benidorm. Con esta apertura la joven marca supera la cifra de los 30 
			restaurantes en España y 3 en la Comunidad Valenciana, fortaleciéndose como referente 
			en el sector y como motor de la economía, gracias a la creación permanente de puestos 
			de trabajo en todo el país.
		 <p>

	<p>
		El nuevo establecimiento se ubica en la calle Calle Tomás Ortuño nº 5 de Benidorm, 
		y tiene una superficie de 386 m2, incluyendo la zona de terraza. La selección exigente en origen, 
		la experiencia en el tratamiento y una suculenta presentación, son la fórmula idónea para que un 
		total de 380 comensales puedan disfrutar de la esencia del concepto creado por La Sureña. 
		La compañía prevé mantener durante el presente ejercicio su curva de aperturas y facturación 
		en progresión ascendente, a pesar de la situación económica actual. En la actualidad, 
		Restalia dispone de más de 200 restaurantes abiertos en nuestro país, que dan empleo a más 
		de 4.000 personas. Concretamente, Cervecería La Sureña, desde sus inicios, ha sabido seguir 
		creciendo con un modelo de negocio sostenido y reportando unas cifras positivas. 
		Según Anne Corcuera, responsable de comunicación de Restalia. 
		Montaditos, que cerrará 2012 con 320 restaurantes.</p>			
		

			
		
	</section><!-- /content -->

	

</div><!-- /page -->



<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.8.3.min.js"><\/script>')</script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(window).resize(function(){
			var ancho = $(window).width();
			var alto = $(window).height();
			console.log('Ancho: ' + ancho);
			console.log('Alto: ' + alto);
		})

	});
	
</script>
</body>
</html>