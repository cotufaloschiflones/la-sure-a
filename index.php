<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<title>La Sureña</title>
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
	<link rel="stylesheet" href="css/styles.css" />
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/fontello-ie7.css">
    <![endif]-->
</head>
<body>
<div class="body" data-role="page">

	<div data-role="content">	
		
			<div class="logo"></div>

			<!-- <hr class="left-hr">
			<hr class="right-hr">
			<section class="rrss">
				<a class="fb" rel="external" href="http://www.lasureña.es"><i class="icon-facebook"></i></a>
				<a class="tw" rel="external" href="http://www.lasureña.es"><i class="icon-twitter"></i></a>
			</section>
			-->

			<section class="contenido">
				<h3 class="center seleccion">Ir a Versión</h3>

				<section class="select_nav">
					
					<a class="pc" rel="external" href="http://www.lasureña.es"></a>
					<a class="movil" data-transition="flip" href="home.php"></a>

				</section>
			</section>
			

			<!-- 
			<a data-transition="slide" href="page2.php">page 2.</a>
			<i class="icon-twitter"></i> 
            <i class="icon-location"></i>
          
            <i class="icon-facebook-rect"></i> 
            <i class="icon-thumbs-up"></i>
          
            <i class="icon-monitor"></i> 
            <i class="icon-mobile"></i> 	 -->	

	</div><!-- /content -->

</div><!-- /page -->


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.8.3.min.js"><\/script>')</script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		//alert('ss')
		$(window).resize(function(){
			var ancho = $(window).width();
			var alto = $(window).height();
			console.log('Ancho: ' + ancho);
			console.log('Alto: ' + alto);
		})
	});
	
</script>
</body>
</html>