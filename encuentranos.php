<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<title>La Sureña</title>
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
	<link rel="stylesheet" href="css/styles.css" />
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/fontello-ie7.css">
    <![endif]-->
	
</head>
<body>

<div class="body" data-role="page">

	<header data-role="header">
		<a href="index.html" data-role="button" data-icon="arrow-l" data-rel="back" data-inline="true" data-theme="c"><span class="enfasis">V</span>olver</a>
		<h1 class="header-top"><span class="enfasis">L</span>a <span class="enfasis">S</span>ureña</h1>
	</header><!-- /header -->

	<section data-role="content" class="content">
		<h1><span class="enfasis">E</span>ncuéntranos</h1>		
		
		<p>Utiliza nuestro buscador, localiza La Sureña que más te interese y ven a disfrutar del Auténtico Sur.</p>

		<p>Según tu posición actual:</p>
					
		<a id="pidoLocation" data-role="button" data-corners="false" class="boton-location"><span class="enfasis">P</span>ermitir <i class="icon-direction"></i></a>

		<p>Por Código Postal, Localidad o Dirección:</p>
		
		<input type="text" data-corners="false"  class="filtrado ui-corner-all">			
		<a href="" class="submit"><i class="icon-right-circle"></i></a>
		

		<div id="log"></div>
			
		</ul>
		
	<!--
		<i class="icon-direction"></i>
		<i class="icon-left-circle"></i>
		<i class="icon-right-circle"></i>
		<i class="icon-address"></i>
		<i class="icon-location"></i>
		<i class="icon-location-1"></i>
		<i class="icon-location-inv"></i>
		<i class="icon-compass"></i>
		<i class="icon-zoom-in"></i>
		<i class="icon-zoom-out"></i>
		<i class="icon-spin2"></i>
		<i class="icon-spin3"></i>
		<i class="icon-globe-alt"></i>
		<i class="icon-monitor"></i>
		<i class="icon-link-2"></i>
		<i class="icon-mobile"></i>
	-->

	</section><!-- /content -->




	

</div><!-- /page -->


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.8.3.min.js"><\/script>')</script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		
		$('pidoLocation').on('click', getLocation());

		var log=document.getElementById("log");

		function getLocation(){
		  if (navigator.geolocation){
		    navigator.geolocation.getCurrentPosition(showPosition);

		  }else{
		  	log.innerHTML="Tu dispositivo no soporta Geolocation.";
		  }
		}

		

		function showPosition(position){
		  	log.innerHTML="Latitud: " + position.coords.latitude + "<br>Longitud: " + position.coords.longitude;
		  	

		  	//current

		  	// TODO: TRAER DE LA BBDD EL OBJETO CON TODOS LOS LOCALES, GUARDARLO EN LOCALSTORAGE (EN UNA VARIABLE) Y USARLO AHÍ, RECORRERLO CON FOR Y ORDERNARLO.
		  	// FIXME: ARREGLAME
		  	// NOTE: es una nota
		  	$.ajax({
				type: "GET",
				url: "http://lasurena.es/includes/genera_locales.php",
				dataType: 'json',
				success :  comparoCercania,
				error : muestroError		
			});
		 	

			/*

			var size = Object.size(data);
	
			for (i=0; i<size; i++){
			}

			*/

			//http://lasurena.com/includes/genera_locales.php

		  	var lat1 = position.coords.latitude;
			var lon1 = position.coords.longitude;

			var lat2 = 40.465284;
			var lon2 = -3.758414;

		 	calcularDistancia(lat1, lon1, lat2, lon2);
		  
		  //return p1;
		}



		function comparoCercania (data) {

			console.log(data)

			var size = Object.size(data);

			console.log('data')

		}

		function muestroError (jqXHR) {
			console.log(jqXHR)
		}
		


		function calcularDistancia(lat1, lon1, lat2, lon2) {
		  var R = 6371; // km
		  var dLat = toRad(lat2 - lat1);
		  var dLon = toRad(lon2 - lon1); 
		  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
		          Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
		          Math.sin(dLon / 2) * Math.sin(dLon / 2); 
		  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)); 
		  var d = R * c;

		  var km = d;
		  var m = km*1000;

		  console.log(km + ' km');
		}



		function toRad(dato){
			return dato * Math.PI / 180;
		}





	});



	
</script>
</body>
</html>