<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<title>La Sureña</title>
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
	<link rel="stylesheet" href="css/styles.css" />
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/fontello-ie7.css">
    <![endif]-->
	
</head>
<body>

<div class="body" data-role="page">

	<section data-role="content">	
		
			<div class="logo"></div>

			<section class="contenido">			

					<nav data-role="controlgroup" class="main-menu" >
						<ul data-role="listview" data-inset="true" >
							<li data-icon="arrow-r"><a href="autentico-sur.php" data-transition="slide"><span>A</span>uténtico <span>S</span>ur</a></li>
							<li data-icon="arrow-r"><a href="noticias.php" data-transition="slide"><span>N</span>oticias</a></li>
							<li data-icon="arrow-r"><a href="nuestra-carta.php" data-transition="slide"><span>N</span>uestra <span>C</span>arta</a></li>
							<li data-icon="arrow-r"><a href="nuestras-promos.php" data-transition="slide"><span>N</span>uestras <span>P</span>romos</a></li>
							<li data-icon="arrow-r"><a href="encuentranos.php" data-transition="slide"><span>E</span>ncuéntranos</a></li>
							<li data-icon="arrow-r"><a href="mailto:attcliente@gruporestalia.com" target="_blank" data-transition="slide"><span>C</span>ontacto</a></li>
						</ul>
					</nav>
			</section>
			

			<!-- 
			<a data-transition="slide" href="page2.php">page 2.</a>
			<i class="icon-twitter"></i> 
            <i class="icon-location"></i>
          
            <i class="icon-facebook-rect"></i> 
            <i class="icon-thumbs-up"></i>
          
            <i class="icon-monitor"></i> 
            <i class="icon-mobile"></i> 	 -->	

	</section><!-- /content -->

</div><!-- /page -->



<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.8.3.min.js"><\/script>')</script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){

		//alert('ss')

	});
	
</script>
</body>
</html>