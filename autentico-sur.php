<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<title>La Sureña</title>
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
	<link rel="stylesheet" href="css/styles.css" />
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/fontello-ie7.css">
    <![endif]-->
	
</head>
<body>

<div class="body" data-role="page">

	<div data-role="header">
		<a href="index.html" data-role="button" data-icon="arrow-l" data-rel="back" data-inline="true" data-theme="c"><span class="enfasis">V</span>olver</a>
		<h1 class="header-top"><span class="enfasis">L</span>a <span class="enfasis">S</span>ureña</h1>
	</div><!-- /header -->

	<div data-role="content" class="content">
		<h1>¿<span class="enfasis">Q</span>ué es <span class="enfasis">L</span>a <span class="enfasis">S</span>ureña?</h1>		
		<p>
			La Sureña es más que un restaurante, 
			una taberna o un chiringuito de playa. 
			La Sureña es un espacio que te transporta al Auténtico Sur. 
			Un lugar de encuentro que te hace viajar y evoca libertad 
			y optimismo. Un espacio donde puedes 
			ser tu mismo.
		</p>	
		<p>
			La sureña es capaz de unir a esa buena gente alrededor de una mesa, alrededor de un cubo, 
			un cubo con 5 botellines de cerveza y una variedad de raciones del mar y de la tierra. 
			La Sureña es el norte, el este, el oeste y sobre todo, es el SUR; 
			Pero es un Sur que va más allá de lo típico y lo tópico. 
			Es una forma de entender la vida.</p>
	</div><!-- /content -->

	

</div><!-- /page -->



<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.8.3.min.js"><\/script>')</script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(window).resize(function(){
			var ancho = $(window).width();
			var alto = $(window).height();
			console.log('Ancho: ' + ancho);
			console.log('Alto: ' + alto);
		})

	});
	
</script>
</body>
</html>