<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<title>La Sureña</title>
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
	<link rel="stylesheet" href="css/styles.css" />
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/fontello-ie7.css">
    <![endif]-->
	
</head>
<body>

<div class="body" data-role="page">

	<header data-role="header">
		<a href="index.html" data-role="button" data-icon="arrow-l" data-rel="back" data-inline="true" data-theme="c"><span class="enfasis">V</span>olver</a>
		<h1 class="header-top"><span class="enfasis">L</span>a <span class="enfasis">S</span>ureña</h1>
	</header><!-- /header -->

	<section data-role="content" class="content">
		<h1><span class="enfasis">N</span>uestra <span class="enfasis">C</span>arta</h1>

		<section class="tipo-plato">
			<aside>
				<h4><span>R</span>aciones</h4>
				<div>Entera - Media</div>
			</aside>
			<ul>

				<li><h5><a href="imagen.php" data-imagen="paleta-iberica.jpeg" data-transition="fade">Gamba Blanca - 200gr</a><h5><span class="precio media">3€</span><span> - </span><span class="precio racion">5€</span></li>
				<li><h5><a href="imagen.php" data-imagen="paleta-iberica2.jpeg" data-transition="fade">Paletilla de Jamón Ibérico - 250gr</a><h5><span class="precio media">3€</span><span> - </span><span class="precio racion">5€</span></li>
				<li><h5><a href="imagen.php" data-imagen="paleta-iberica3.jpeg" data-transition="fade">Carne Mechada - 240gr</a><h5><span class="precio media">3€</span><span> - </span><span class="precio racion">5€</span></li>
			</ul>
		</section>


		<section class="tipo-plato">
			<aside>
				<h4><span>T</span>inglaos</h4>
				<div>Entera - Media</div>
			</aside>
			<ul>

				<li><h5><a href="">Gamba Blanca - 200gr</a><h5><span class="precio media">3€</span><span> - </span><span class="precio racion">5€</span></li>
				<li><h5><a href="">Gamba Blanca - 200gr</a><h5><span class="precio media">3€</span><span> - </span><span class="precio racion">5€</span></li>

			</ul>
		</section>
		
		
	</section><!-- /content -->

	

</div><!-- /page -->



<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.8.3.min.js"><\/script>')</script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(window).resize(function(){
			var ancho = $(window).width();
			var alto = $(window).height();
			console.log('Ancho: ' + ancho);
			console.log('Alto: ' + alto);
		});

		var $imagen = $('.tipo-plato ul li a');

		$imagen.on('click', muestraImagen);

		function muestraImagen () {
			var url = $(this).data('imagen');
			var titulo = $(this).text();

			$.post('imagen.php', {url:url, titulo:titulo});

			console.log(url + '-- & --' + titulo)
		}

	});
	
</script>
</body>
</html>